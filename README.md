SSI Java Libraries project summary
==================================

## Introduction

Danube Tech has been building SSI technologies and solutions for several years, for example
by launching the Universal Resolver open-source project at DIF, or by participating in the
eSSIF-Lab Business call, where we worked on a SaaS platform for creating and resolving DIDs.

In these various efforts, we have consistently relied on a set of basic open-source components
that implement DIDs, Verifiable Credentials, and related building blocks for the SSI ecosystem.
These independently implemented components include **jsonld-common-java**, **did-common-java**,
**ld-signatures-java**, **verifiable-credentials-java**, **key-formats-java**.

All of them need more work
in several areas, and additional components are currently being planned. In this project, we
will improve and complete these libraries, which will result in the availability of a high-quality
and generic set of open-source Java components for higher-level SSI applications and services.

## Libraries

| Library Name | Description | License | Latest Release |
| ----------- | -------------- | ---------------- | ----------------------- |
| [jsonld-common-java](https://github.com/decentralized-identity/jsonld-common-java) | Helper objects and functions for [JSON-LD](https://w3c.github.io/json-ld-syntax/) documents. | Apache 2 | 1.0.0 |
| [did-common-java](https://github.com/decentralized-identity/did-common-java) | Implementation of the [DID Core](https://www.w3.org/TR/2021/CR-did-core-20210318/) specification. | Apache 2 | 1.0.0 |
| [ld-signatures-java](https://github.com/weboftrustinfo/ld-signatures-java) | Implementation of cryptographic suites for [Linked Data Proofs](https://w3c-ccg.github.io/ld-proofs/). | Apache 2 | 1.0.0 |
| [verifiable-credentials-java](https://github.com/danubetech/verifiable-credentials-java) | Implementation of the [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/) data model. | Apache 2 | 1.0.0 |
| [key-formats-java](https://github.com/danubetech/key-formats-java/) | Implementation of various key formats used by DIDs and Verifiable Credentials. | Apache 2 | 1.2.0 |
| [cborld-java](https://github.com/danubetech/cborld-java) | Implementation of the CBOR serialization of JSON-LD. | Apache 2 | 1.0.0 |
| [didcomm-java](https://github.com/danubetech/didcomm-java) | Implementation of the [DIDComm v2 protocol](https://identity.foundation/didcomm-messaging/spec/). | Apache 2 | 0.1-SNAPSHOT |
